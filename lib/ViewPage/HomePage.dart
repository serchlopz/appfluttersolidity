import 'package:flutter/material.dart';
import 'package:flutteruiapp/ViewPage/Web3Smartcontract.dart';
import 'AddProveedorPage.dart';
import 'GetProveedorPage.dart';
import 'WalletPage.dart';
import 'ApiRestDart.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(
          "Flutter DApp",
          style: new TextStyle(fontSize: 30),
          textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.grey,
        iconTheme: new IconThemeData(color: Colors.white),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("¡Bienvenido Sergio!", style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.indigo[300])),
            SizedBox(height: 40),
            RawMaterialButton(
              padding: EdgeInsets.only(
                  left: 70, top: 20, right: 70, bottom: 20),
              child: Text("Registro Proveedor",
                  style:
                      TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
              elevation: 5.0,
              highlightElevation: 19.0,
              focusColor: Colors.black,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Proveedor(),
                    ));
              },
              fillColor: Colors.teal[600],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
            ),
            SizedBox(height: 20),
            RawMaterialButton(
              padding:EdgeInsets.only(
                  left: 65, top: 20, right: 65, bottom: 20),
              child: Text("Consultar Proveedor",
                  style:
                      TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
              elevation: 5.0,
              highlightElevation: 19.0,
              focusColor: Colors.black,
              onPressed: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RequestProveedor(),
                        ));
              },
              fillColor: Colors.teal[500],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
            ),
           SizedBox(height: 20),
            RawMaterialButton(
              padding:  EdgeInsets.only(
                  left: 130, top: 20, right: 130, bottom: 20),
              child: Text("Wallet",
                  style:
                      TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
              elevation: 5.0,
              highlightElevation: 19.0,
              focusColor: Colors.black,
              onPressed: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Wallet(),
                        ));
                // Navigator.of(context).pushReplacement( //Remplazar una pagina al ingresar, no return
                //     MaterialPageRoute(builder: (context) => Wallet()));
              },
              fillColor: Colors.teal[400],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
            ),
             SizedBox(height: 20),
            RawMaterialButton(
              padding:  EdgeInsets.only(
                  left: 120, top: 20, right: 120, bottom: 20),
              child: Text("Api Rest",
                  style:
                      TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
              elevation: 5.0,
              highlightElevation: 19.0,
              focusColor: Colors.black,
              onPressed: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ApiRestClass(),
                        ));
                // Navigator.of(context).pushReplacement( //Remplazar una pagina al ingresar, no return
                //     MaterialPageRoute(builder: (context) => Wallet()));
              },
              fillColor: Colors.teal[300],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
            ),
            SizedBox(height: 20),
            RawMaterialButton(
              padding:  EdgeInsets.only(
                  left: 80, top: 20, right: 80, bottom: 20),
              child: Text("Smart Contracts",
                  style:
                      TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
              elevation: 5.0,
              highlightElevation: 19.0,
              focusColor: Colors.black,
              onPressed: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SmartContractClass(),
                        ));
                // Navigator.of(context).pushReplacement( //Remplazar una pagina al ingresar, no return
                //     MaterialPageRoute(builder: (context) => Wallet()));
              },
              fillColor: Colors.teal[200],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
            ),
            SizedBox(height: 60),
            Text("Desing in BLOXITY company", style: TextStyle(fontSize: 15, color: Colors.grey, fontStyle: FontStyle.italic)), 
          ],
        ),
      ),
    );
  }
}
