import 'package:flutter/material.dart';

class RequestProveedor extends StatelessWidget {
  String _rfc;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.grey,
        title: new Text(
          "Consultar Proveedor",
          style: new TextStyle(fontSize: 20.0),
          textAlign: TextAlign.center
        ),
        iconTheme: new IconThemeData(color: Colors.white),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20),
            Text("Ingrese el RFC a consultar en la Blockchain", style:TextStyle(fontSize: 17)),
            SizedBox(height: 20),
            new TextFormField(
          decoration: const InputDecoration(
            labelText: 'RFC Proveedor:',
            icon: const Icon(Icons.details),
            ),
          keyboardType: TextInputType.phone,
          // validator: validateRFC,
          onSaved: (String val) {
            _rfc = val;
          },
        ),

        SizedBox(height: 60.0),
            RawMaterialButton(
              padding: const EdgeInsets.only(
                  left: 88, top: 20, right: 88, bottom: 20),
              child: Text("Consultar",
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),elevation: 5.0, highlightElevation: 19.0,
              onPressed: () {},
              fillColor: Colors.teal,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
            ),
          ],
        ),
      ),
    );
  }
}
