import 'dart:io';
import 'package:flutter/material.dart';
import 'package:laksadart/laksadart.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'dart:convert';


class ApiRestClass extends StatefulWidget {
  ApiRestClass({Key key}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<ApiRestClass> {
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: new Text("Api Rest"),
          backgroundColor: Colors.grey,
          iconTheme: new IconThemeData(color: Colors.white),
        ),

        body: new SingleChildScrollView(
          child: new Container(
            margin: new EdgeInsets.all(60),
            child: new Form(
              key: globalKey,
              autovalidate: _autoValidate,
              child: FormUI(),
            ),
          ),
        ),
    );
  }

  Widget FormUI() {
    return new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
                    new TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'id Proveedor:',
                    icon: const Icon(Icons.details),
                    ),
                  keyboardType: TextInputType.phone,
                  validator: validateid,
                  onSaved: (String val) {
                    valorID = val;
                  },
                ),
                SizedBox(height: 20),
                Icon(Icons.vpn_key),
                Text("Resultado:", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                Text('$textRestResponse', style: TextStyle(fontSize: 20)),
                SizedBox(height: 20),
                  RawMaterialButton(
                  shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.all(new Radius.circular(10)),
                  ),
                  textStyle: TextStyle(
                    fontSize: 20,
                    color: Colors.black
                  ),
                  fillColor: Colors.teal[300],
                  elevation: 5.0,
                  highlightElevation: 15.0,
                  padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                  child: Text("Generar Petición"),
                  onPressed: (){_validateInputs();},
                ),
                
                
                
      ]
    );
  }
 
  String valorID;
  bool _autoValidate = false;
  final GlobalKey<FormState> globalKey = GlobalKey<FormState>();

  String validateid(String value) {
    if (value.length == 0) {
      return 'id es requerido';
    }
      return null;
  }

  void _validateInputs() {
      if (globalKey.currentState.validate()){
          globalKey.currentState.save();
          obtenerIdByUser();
      } 
      else {
          setState(() {
            _autoValidate = true;
          });
        }
      }

  void obtenerIdByUser(){
   try {     
      setState(() {
      String url = 'https://jsonplaceholder.typicode.com/posts';
      getPost(url,valorID);
   });   
  } 
    catch (e) {
      print(e.toString());
    } 
  }
}

class Post {
    int userId;
    int id;
    String title;
    String body;

    Post({this.userId, this.id, this.title, this.body});
  

    factory Post.fromJson(Map<String, dynamic> json) => new Post(
        userId: json["userId"],
        id: json["id"],
        title: json["title"],
        body: json["body"],
    );
    Map<String, dynamic> toJson() => {
        "userId": userId,
        "id": id,
        "title": title,
        "body": body,
    };
    
}
String textRestResponse;


String postToJson(Post data) {
    final dyn = data.toJson();
    return json.encode(dyn);
}
Future<Post> getPost(String url, String id) async{
  final response = await http.get('$url/$id');
  //print(response.body.toString());
   
  return postFromJson(response.body);
  
}
Post postFromJson(String str) {
    final jsonData = json.decode(str);
    print(jsonData);
    jsonData.toString();
    textRestResponse = Post.fromJson(jsonData).title.toString();
    return Post.fromJson(jsonData);
}

