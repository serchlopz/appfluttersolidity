import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'HomePage.dart';
class SplashScreen extends StatefulWidget {
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    loadData();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.white,
      // decoration: BoxDecoration(
      //   image: DecorationImage(
      //        image: AssetImage('assets/girl.jpeg'),
      //          fit: BoxFit.cover
      //    ) ,
      // ),
      child: Center(
        child: 
        CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.teal,
          ),
        )
      )
      //   child: new Dialog(
      //   child: new Row(
      //     mainAxisSize: MainAxisSize.min,
      //     children: [
      //       new CircularProgressIndicator(),
      //       new Text("Loading"),
      //     ],
      //   ),
      // ),flutter
    );
  }
  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 2), onDoneLoading);
  }
  onDoneLoading() async {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomePage()));
  }
}
