import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutteruiapp/LogicPage/ProveedorLogic.dart';
import 'package:laksadart/laksadart.dart';

class Proveedor extends StatefulWidget {
  Proveedor({Key key}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Proveedor> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: new Text("Registro de Proveedor"),
          backgroundColor: Colors.grey,
          iconTheme: new IconThemeData(color: Colors.white),
        ),

        body: new SingleChildScrollView(
          child: new Container(
            margin: new EdgeInsets.all(15.0),
            child: new Form(
              key: globalKey,
              autovalidate: _autoValidate,
              child: FormUI(),
            ),
          ),
        ),
    );
  }

  Widget FormUI() {
    return new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: new Text(
                          "Nuevo Proveedor",
                        style: new TextStyle(fontSize: 25.0),
                    ),
                  )
                ],
              ),
        SizedBox(height: 20),
        new TextFormField(
          decoration: const InputDecoration(
            labelText: 'Razon Social',
            icon: const Icon(Icons.business),
            ),
          keyboardType: TextInputType.text,
          validator: validateRazonSocial,
          onSaved: (String val) {
            _razonSocial = val;
          },
        ),
        SizedBox(height: 20),

        new TextFormField(
          decoration: const InputDecoration(
            labelText: 'Entidad Federativa',
            icon: const Icon(Icons.person_pin_circle)
            
            ),
          keyboardType: TextInputType.text,
          validator: validateEntidadFed,
          onSaved: (String val) {
            _entidadFederativa = val;
          },
        ),

        SizedBox(height: 20),
        
        new TextFormField(
          decoration: const InputDecoration(
            labelText: 'RFC',
            icon: const Icon(Icons.details),
            ),
          keyboardType: TextInputType.phone,
          validator: validateRFC,
          onSaved: (String val) {
            _rfc = val;
          },
        ),
        SizedBox(height: 20),
         new TextFormField(
          decoration: const InputDecoration(
            labelText: 'Address',
            icon: const Icon(Icons.account_balance_wallet),
            ),
          keyboardType: TextInputType.phone,
          validator: validateAddress,
          onSaved: (String val) {
            _address = val;
          },
        ),

        SizedBox(height: 60.0),

        RawMaterialButton(
          padding: const EdgeInsets.only(
            left: 88, top: 15, right: 88, bottom: 15),
          child: Text("Registrar", 
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
            elevation: 5.0,
            highlightElevation: 19.0,
            focusColor: Colors.black,
          onPressed: (_addProveedorToSolidity),
          fillColor: Colors.teal,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30)),
        ),
        SizedBox(height: 60.0)
      ]
    );
  }


  //--------------------------------------------------------startLogic
  final GlobalKey<FormState> globalKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _entidadFederativa = "Metepec";
  String _razonSocial ="Boxity";
  String _rfc= "BLX";
  String _address = "0xbFd2C06be67b9Cb50b2714EecBe2531544f53FdC";
  //---------------------------------------------------------endLogic


  //------------------------------------------------------------------------startWEB3
  static String adrsContract = "0xbFd2C06be67b9Cb50b2714EecBe2531544f53FdC";
  static String abi = "[{'constant':true,'inputs':[{'name':'_RFC','type':'string'}],'name':'RequestProveedor','outputs':[{'name':'','type':'bool'}],'payable':false,'stateMutability':'view','type':'function'},{'constant':false,'inputs':[{'name':'_RFC','type':'string'},{'name':'_razonSocial','type':'string'},{'name':'_domicilio','type':'string'}],'name':'AddProveedor','outputs':[],'payable':false,'stateMutability':'nonpayable','type':'function'}]";
  
  static String adrsWalletMetaMask ="0x9A5C04C4B530673A388c71D78B72AC3b3e92F72e";
  
  static String url_infuraMainNet = "https://mainnet.infura.io";
  static String url_infiraRopsten = "ropsten.infura.io/v3/0efbc68ea1984214bbbe73a87aa8d354";
  static String url_ZilliqaMainNet = "https://api.zilliqa.com/";
  static String url_ZilliqaTestNet = "https://dev-api.zilliqa.com";

  static String secretsProyect = "73a855e9567642f792c3823d1d6c1407";
  //static String rpcUrl_ = "u0l3wmjvgx-u0xq836oi0-rpc.us0-aws.kaleido.io";
  // static String user_ = "u0i6n7wxhm";
  // static String pass_ = "ZwPQBWbVbsPr7D_6AYUJlaNmZMOSo7CLQcch9Q_TPr8";
  // static String nodeurl_ = "https://$user_:$pass_@$rpcUrl_";
  
  Laksa laksa = new Laksa(nodeUrl:url_ZilliqaTestNet);
  
  
  
  //----------------------------------------------------------------------------endWEB3

    void _validateInputs() {
      if (globalKey.currentState.validate()){
          globalKey.currentState.save();
          //_addProveedorToSolidity();
      } 
      else {
          setState(() {
            _autoValidate = true;
          });
        }
      }

      void _addProveedorToSolidity(){
        File contract = new File('documents/proyects/contracts/Proveedor.sol');
        String poryectId = "0efbc68ea1984214bbbe73a87aa8d354";
        String idSecret = "73a855e9567642f792c3823d1d6c1407";
        String url= "ropsten.infura.io/v3/0efbc68ea1984214bbbe73a87aa8d354";

              contract.readAsString().then((contractString) async {
              var abi = [{'constant':'true','inputs':[{'name':'_RFC','type':'string'}],'name':'RequestProveedor','outputs':[{'name':'','type':'bool'}],'payable':'false','stateMutability':'view','type':'function'},{'constant':'false','inputs':[{'name':'_RFC','type':'string'},{'name':'_razonSocial','type':'string'},{'name':'_domicilio','type':'string'}],'name':'AddProveedor','outputs':[],'payable':'false','stateMutability':'nonpayable','type':'function'}];

              var x = laksa.contracts.wallet;
              print(x);
                  //laksa.wallet.add('e19d05c5452598e24caad4a0d85a49146f7be089515c905ae6a19e8a578a6930');
                  laksa.wallet.add(adrsWalletMetaMask);
                  var newContract = laksa.contracts.newContract(code: contractString, init: abi, version: 0);
                  newContract.setDeployPayload(gasLimit: 10000, gasPrice: BigInt.from(1000000000), toDS: true);
                  var sent = await newContract.sendContract();
                  print(sent.transaction.toPayload);
                  var sendTime = DateTime.now();
                  print('sent contract at:$sendTime');
                  print(sent.transaction.TranID);
                  var deployed = await sent.confirmTx(maxAttempts: 33, interval: 1000);
                  print(deployed.ContractAddress);
                  
                  if (deployed != null) {
                    var during = DateTime.now().difference(sendTime);
                    print('deployed confirmed during:$during');
                  }
            });
      }     
            
              //var t = laksa.contracts.wallet.importAccountFromMnemonic('phrase', 1);
              //var x = laksa.contracts.testContract();
              //laksa.contracts.newContract();
              // Transaction tx;
              // Future<bool>  temp = tx.trackTx(txHash);
              // if(temp == true){
              // }
              //var z = laksa.contracts.getAddressForContract(tx);
              // var a = laksa.setBlockchain();
              // laksa.contracts.testContract();
              // laksa.setContracts();
              //Transaction trans = ´0x9A5C04C4B530673A388c71D78B72AC3b3e92F72e';
              // Contract contrato = new Contract();
              // var s = contrato.ContractAddress.toString();
              //laksa.contracts.getAddressForContract(tx);
              // laksa.contracts.at(contract;)

}