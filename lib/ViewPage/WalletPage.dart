import 'dart:io';
import 'package:flutter/material.dart';
import 'package:laksadart/laksadart.dart';

class Wallet extends StatefulWidget {
  Wallet({Key key}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Wallet> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: new Text("Wallet"),
          backgroundColor: Colors.grey,
          iconTheme: new IconThemeData(color: Colors.white),
        ),

        body: new SingleChildScrollView(
          child: new Container(
            margin: new EdgeInsets.all(80),
            child: new Form(
              child: FormUI(),
            ),
          ),
        ),
    );
  }

  Widget FormUI() {
    return new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
           
                  RawMaterialButton(
                  shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.all(new Radius.circular(10)),
                  ),
                  textStyle: TextStyle(
                    fontSize: 20,
                    color: Colors.black
                  ),
                  fillColor: Colors.teal[400],
                  elevation: 5.0,
                  highlightElevation: 15.0,
                  padding: EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 10),
                  child: Text("Generar wallet"),
                  onPressed: (){createWallet();},
                ),
                SizedBox(height: 15),
                Icon(Icons.vpn_key),
                Text("Dirección privada:", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                Text('$adrsPrivate', style: TextStyle(fontSize: 20)),
                SizedBox(height: 30),
                Icon(Icons.vpn_lock),
                Text("Dirección publica:", style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                Text("$adrsPublic", style: TextStyle(fontSize: 20)),
                SizedBox(height: 30),
                Icon(Icons.account_balance_wallet),
                Text("Dirección Wallet:", style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                Text("$addrsWallet", style: TextStyle(fontSize: 20)),
                SizedBox(height: 60),
                 RawMaterialButton(
                  shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.all(new Radius.circular(10)),
                  ),
                  textStyle: TextStyle(
                    fontSize: 20,
                    color: Colors.black
                  ),
                  fillColor: Colors.teal[400],
                  elevation: 5.0,
                  highlightElevation: 15.0,
                  padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                  child: Text("Consultar Bloque"),
                  onPressed: (){getNumBloque();},
                ),
                SizedBox(height: 15),
                Icon(Icons.add_box),
                Text("Numero de Bloque", style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                Text("$block_Temp", style: TextStyle(fontSize: 20)),
      ]
    );
  }
  static String url_ZilliqaTestNet = "https://dev-api.zilliqa.com";
  static String rpcUrl_ = "u0l3wmjvgx-u0xq836oi0-rpc.us0-aws.kaleido.io";
  static String user ="u1sv25kpkj";
  static String pass= "OM3ULCheUMT6hK92mLNMEmh28zWIoHnWEOljKdKeJZc";
  static String nodeUrl= "https://$user:$pass@$rpcUrl_";
  Laksa laksa = new Laksa(nodeUrl:url_ZilliqaTestNet);
  // static String user_ = "u0i6n7wxhm";
  // static String pass_ = "ZwPQBWbVbsPr7D_6AYUJlaNmZMOSo7CLQcch9Q_TPr8";
  // static String nodeurl_ = "https://$user_:$pass_@$rpcUrl_";
  // static String user2 = "Bloxity01";
  // static String pass2 = "0x9A5C04C4B530673A388c71D78B72AC3b3e92F72e";
  // static String nodeurl_ = "https://$user2:$pass2@$rpcUrl_";
  
  String url = "https://mainnet.infura.io";
  String bloque;
  String walletString;
  String address;
  String adrsPublic;
  String addrsWallet;
  String balanceWallet;
  String adrsPrivate;
  String adrsObtenida;
  
  Account acc;
  String block_Temp; 
  String bloqueToShow; 

  //static Laksa laksa = new Laksa(nodeUrl: nodeUrl, networkID: 'sergioNet' );
  
  void createWallet(){  
  try{
        setState(() {
            Account newAcc = laksa.wallet.create();
            newAcc.encryptAccount('Sergio Lopez');
            adrsPrivate = newAcc.privateKey.toString();
            adrsPublic = newAcc.publicKey.toString();
            addrsWallet = newAcc.address.toString();
            balanceWallet = newAcc.balance.toString();
            print(newAcc.toJson());
        });  
    } 
    catch (e){
      print(e.toString());
      throw e;
    }
  }
   void getNumBloque()  {
            try {     
               setState(() {

                 laksa.blockchain.getNumTxBlocks().then((block) => {
                      block_Temp =  block.result.toString(),
                      });
               });   
                      print(block_Temp);
                } 
                catch (e) {
                  print(e.toString());
                    }
  }  
  void t(){
     bloqueToShow = block_Temp;
     print(bloqueToShow);
  }

}